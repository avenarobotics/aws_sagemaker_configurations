#!/usr/bin/env python
import os
import argparse
import logging
import sys
import ast
import json
from pathlib import Path

from detectron2.engine import launch
from detectron2.config import get_cfg, CfgNode
from detectron2 import model_zoo
from detectron2.checkpoint import DetectionCheckpointer

# from engine.custom_trainer import Trainer
# from evaluation.coco import D2CocoEvaluator
# import some common libraries
import argparse
import sys
import logging
import os
from collections import OrderedDict
import torch
import json
import shutil
import glob
import yaml
from torch.nn.parallel import DistributedDataParallel

# import some common detectron2 utilities
# TODO: check imports and remove redundant
import detectron2.utils.comm as comm
from detectron2 import model_zoo
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.config import get_cfg
from detectron2.modeling import build_model
from detectron2.checkpoint import DetectionCheckpointer, PeriodicCheckpointer
from detectron2.solver import build_lr_scheduler, build_optimizer
from detectron2.data import MetadataCatalog
from detectron2.engine import DefaultTrainer, default_argument_parser, default_setup, hooks, launch
from detectron2.evaluation import (
    CityscapesInstanceEvaluator,
    CityscapesSemSegEvaluator,
    COCOEvaluator,
    COCOPanopticEvaluator,
    DatasetEvaluators,
    LVISEvaluator,
    PascalVOCDetectionEvaluator,
    SemSegEvaluator,
    inference_on_dataset,
    print_csv_format,
)
from detectron2.utils.events import (
    CommonMetricPrinter,
    EventStorage,
    JSONWriter,
    TensorboardXWriter,
)

from detectron2.data import (
    MetadataCatalog,
    build_detection_test_loader,
    build_detection_train_loader,
)
import detectron2.utils.comm as comm
from collections import OrderedDict
from detectron2.data.datasets import register_coco_instances
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, build_detection_test_loader
from detectron2.engine import DefaultTrainer, default_setup, hooks, launch, DefaultPredictor
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.engine import HookBase
from detectron2.data import build_detection_train_loader
from detectron2.modeling import GeneralizedRCNNWithTTA
from detectron2.projects import point_rend
import logging
import torch
import detectron2.modeling.roi_heads.mask_head
from detectron2.modeling import GeneralizedRCNNWithTTA

from detectron2.utils.logger import setup_logger

LOGGER = logging.Logger("TrainingScript", level=logging.INFO)
HANDLER = logging.StreamHandler(sys.stdout)
HANDLER.setFormatter(logging.Formatter("%(levelname)s | %(name)s | %(message)s"))
LOGGER.addHandler(HANDLER)


def get_label_list():
    with open("R101_init_train/class_names.yaml", 'r', encoding='utf8') as stream:
        try:
            print("loading labels list")
            labels_list = yaml.safe_load(stream)['MODEL']['NAMES']
            print("labels loaded")

            return labels_list
        except yaml.YAMLError as exc:
            print(exc)

def r101_pointrend_cfg(config_file):
    config_file.MODEL.MASK_ON = True
    config_file.MODEL.META_ARCHITECTURE = "GeneralizedRCNN"
    config_file.MODEL.BACKBONE.NAME = "build_resnet_fpn_backbone"
    config_file.MODEL.RESNETS.OUT_FEATURES = ["res2", "res3", "res4", "res5"]
    config_file.MODEL.RESNETS.DEPTH = 101
    config_file.MODEL.FPN.IN_FEATURES = ["res2", "res3", "res4", "res5"]
    config_file.MODEL.ANCHOR_GENERATOR.SIZES = [[32], [64], [128], [256], [512]]
    config_file.MODEL.ANCHOR_GENERATOR.ASPECT_RATIOS = [[0.5, 1.0, 2.0]]
    config_file.MODEL.RPN.IN_FEATURES = ["p2", "p3", "p4", "p5", "p6"]
    config_file.MODEL.RPN.PRE_NMS_TOPK_TRAIN = 2000
    config_file.MODEL.RPN.PRE_NMS_TOPK_TEST = 1000
    config_file.MODEL.RPN.POST_NMS_TOPK_TRAIN = 1000
    config_file.MODEL.RPN.POST_NMS_TOPK_TEST = 1000
    config_file.MODEL.ROI_HEADS.NAME = "StandardROIHeads"
    config_file.MODEL.ROI_HEADS.IN_FEATURES = ["p2", "p3", "p4", "p5"]
    config_file.MODEL.ROI_BOX_HEAD.TRAIN_ON_PRED_BOXES = True
    config_file.MODEL.ROI_BOX_HEAD.NAME = "FastRCNNConvFCHead"
    config_file.MODEL.ROI_BOX_HEAD.NUM_FC = 2
    config_file.MODEL.ROI_BOX_HEAD.POOLER_RESOLUTION = 7
    config_file.MODEL.ROI_MASK_HEAD.NAME = "MaskRCNNConvUpsampleHead"
    config_file.MODEL.ROI_MASK_HEAD.NUM_CONV = 4
    config_file.MODEL.ROI_MASK_HEAD.POOLER_RESOLUTION = 14
    config_file.MODEL.ROI_MASK_HEAD.POOLER_TYPE = ""
    config_file.MODEL.ROI_MASK_HEAD.NAME = "PointRendMaskHead"
    config_file.MODEL.ROI_MASK_HEAD.FC_DIM = 1024
    config_file.MODEL.ROI_MASK_HEAD.NUM_FC = 2
    config_file.MODEL.ROI_MASK_HEAD.OUTPUT_SIDE_RESOLUTION = 7
    config_file.MODEL.ROI_MASK_HEAD.IN_FEATURES = ["p2"]
    config_file.MODEL.ROI_MASK_HEAD.POINT_HEAD_ON = True
    config_file.MODEL.POINT_HEAD.FC_DIM = 256
    config_file.MODEL.POINT_HEAD.NUM_FC = 3
    config_file.MODEL.POINT_HEAD.IN_FEATURES = ["p2"]
    config_file.MODEL.MASK_ON = True
    config_file.SOLVER.BASE_LR = 0.002
    config_file.SOLVER.STEPS = (60000, 80000)
    config_file.SOLVER.MAX_ITER = 90000
    config_file.INPUT.MASK_FORMAT = "bitmask"
    config_file.INPUT.MIN_SIZE_TRAIN = (640, 672, 704, 736, 768, 800)
    config_file.VERSION = 2
    
def _config_training(args: argparse.Namespace) -> CfgNode:

    # load weights path
    weights_list = glob.glob("R101_init_train/*.pkl")
    ##add point rend init weights###
    assert len(
        weights_list) == 1, 'ERROR: Exactly one .pth weights file has to be present in detectron2_inference_weights ' \
                            'directory '
    INIT_WEIGHTS = weights_list

    # load config file path
    config_list = glob.glob("R101_init_train/*.pkl")
    config_list = [yaml_file for yaml_file in config_list if 'class_names' not in yaml_file]
    assert len(config_list) == 1, 'ERROR: Exactly one .yaml file has to be present in detectron2_configs directory, ' \
                                  'not counting class_names.yaml '
    cfg = get_cfg()
    print(os.listdir("/opt/ml/input/data/"))
    print(os.listdir("/opt/ml/input/data/dataset/"))
    
    register_coco_instances("train_set", {},"/opt/ml/input/data/dataset/dataset/train/train.json","/opt/ml/input/data/dataset/dataset/train")
          
    register_coco_instances("val_set", {}, "/opt/ml/input/data/dataset/dataset/val/val.json","/opt/ml/input/data/dataset/dataset/val")

    MetadataCatalog.get("train_set").set(
        thing_classes=get_label_list())
    MetadataCatalog.get("val_set").set(
        thing_classes=get_label_list())

    ######################################################
    ########################POINTREND#####################
    point_rend.add_pointrend_config(cfg)
    r101_pointrend_cfg(cfg)
    point_rend.add_pointrend_config(cfg)
    cfg.MODEL.WEIGHTS = INIT_WEIGHTS[0]
    cfg.MODEL.POINT_HEAD.NUM_CLASSES = 38
    # True if point head is used.
    cfg.MODEL.ROI_MASK_HEAD.POINT_HEAD_ON = True
    ######################################################
    ######################################################

    cfg.SOLVER.IMS_PER_BATCH = 4
    cfg.OUTPUT_DIR = os.environ['SM_MODEL_DIR']
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 38
    cfg.SOLVER.CHECKPOINT_PERIOD = 2000
    cfg.DATALOADER.NUM_WORKERS = int(os.environ["SM_NUM_CPUS"])
    cfg.MODEL.BACKBONE.FREEZE_AT = 0
    # Sample size of smallest side by choice or random selection from range give by
    cfg.INPUT.MIN_SIZE_TRAIN_SAMPLING = "choice"
    cfg.INPUT.MIN_SIZE_TEST_SAMPLING = "choice"
    cfg.INPUT.MIN_SIZE_TRAIN = (800,)
    cfg.INPUT.MIN_SIZE_TEST = (800,)
    cfg.INPUT.MAX_SIZE_TRAIN = 1300
    cfg.INPUT.MAX_SIZE_TEST = 1300
    cfg.MODEL.FPN.NORM = ""
    cfg.MODEL.ROI_MASK_HEAD.NORM = ""
    cfg.MODEL.ROI_BOX_HEAD.NORM = ""
    cfg.MODEL.RESNETS.NORM = "FrozenBN"
    cfg.DATASETS.TRAIN = ("train_set",)
    cfg.DATASETS.TEST = ("val_set",)
    cfg.DATASETS.VAL = ("val_set",)
    cfg.SOLVER.BASE_LR = 0.0002 # pick a good LearningRate
    cfg.SOLVER.STEPS = (300, 700)
    cfg.SOLVER.GAMMA = 0.5
    cfg.SOLVER.WARMUP_FACTOR = 1.0 / 1000
    cfg.SOLVER.WARMUP_ITERS = 100
    cfg.SOLVER.WARMUP_METHOD = "linear"
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
    cfg.SOLVER.MAX_ITER = 10000  # No. of iterations
    # cfg.TEST.EVAL_PERIOD = 500  # No. of iterations after which the Validation Set is evaluated.
    default_setup(cfg, '')
    return cfg


def _train_impl(args) -> None:

    cfg = _config_training(args)
    cfg.setdefault("VAL_LOG_PERIOD", args.log_period)
    trainer = DefaultTrainer(cfg)
    trainer.resume_or_load(resume=False)

    if cfg.MODEL.DEVICE != "cuda":
        err = RuntimeError("A CUDA device is required to launch training")
        LOGGER.error(err)
        raise err
    trainer.train()



def train(args: argparse.Namespace) -> None:
    r"""Launch distributed training by using Detecton2's `launch()` function

    Parameters
    ----------
    args : argparse.Namespace
        training script arguments, see :py:meth:`_parse_args()`
    """
    machine_rank = args.hosts.index(args.current_host)
    LOGGER.info(f"Machine rank: {machine_rank}")
    master_addr = args.hosts[0]
    master_port = "55555"

    url = "auto" if len(args.hosts) == 1 else f"tcp://{master_addr}:{master_port}"
    LOGGER.info(f"Device URL: {url}")

    launch(
        _train_impl,
        num_gpus_per_machine=int(args.num_gpus),
        num_machines=len(args.hosts),
        dist_url=url,
        machine_rank=machine_rank,
        args=(args,),
    )




def _parse_args() -> argparse.Namespace:
    r"""Define training script API according to the argument that are parsed from the CLI

    Returns
    -------
    argparse.Namespace
        training script arguments, execute $(python $thisfile --help) for detailed documentation
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--num-iter",
        type=int,
        default=1000,
        metavar="I",
        help="Maximum number of iterations (default: 1000)",
    )
    parser.add_argument(
        "--log-period",
        type=int,
        default=40,
        help="Occurence in number of iterations at which loss values are logged",
    )

    parser.add_argument("--num-gpus", type=int, default=int(os.environ["SM_NUM_GPUS"]))
    parser.add_argument(
        "--hosts", type=str, default=ast.literal_eval(os.environ["SM_HOSTS"])
    )
    parser.add_argument(
        "--current-host", type=str, default=os.environ["SM_CURRENT_HOST"]
    )
    return parser.parse_args()


if __name__ == "__main__":
    train(_parse_args())



